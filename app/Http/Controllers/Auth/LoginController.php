<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest', ['except' => 'logout']);
    }

    public function Login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|exists:users,username',
            'password' => 'required',
        ], [
            'username.required' => 'Username tidak boleh kosong !',
            'username.exists' => 'Username tidak tersedia !',
            'password.required' => 'Password tidak boleh kosong !'
        ]);

        if ($validator->fails()) 
        {
            $result = array('msg' => 'Username tidak tersedia !');
        } 
        else 
        {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) 
            {
                $result = array('msg' => 'success');
            }
            else 
            {
                $result = array('msg' => 'Password tidak benar, silakan ulangi !');
            }
        }
        //return message to view
        echo json_encode($result);
    }
}
