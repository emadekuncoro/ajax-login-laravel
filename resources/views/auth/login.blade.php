@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <div id="msg" class="alert alert-warning text-center">
                                    <strong></strong>
                    </div>
                    <form class="form-horizontal" id="login_form" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button id="btn-login" type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script>
    $(function () {
        $('#msg').hide();
        $('#login_form').submit(function(event) {
            var post_data   = $(this).serialize();
            var url         = $(this).attr('action');
            $('#btn-login').text('Please wait...');
            $.post(url, post_data, function(data, textStatus, xhr) {
                /*optional stuff to do after success */
                var res = jQuery.parseJSON(data);
                if(res.msg != 'success')
                {
                    $('#btn-login').text('Login');
                    $('#msg').show();
                    $('#msg strong').text(res.msg);
                }
                else
                {
                    location.reload();
                }
            });
            
            return false;
        });
    });
</script>

@endsection


